# ¿Por qué un API REST con Flask?

Como ya sabrás, Flask es un framework para desarrollo web escrito en Python. Se puede utilizar para diversos tipos de aplicación, entre ellas, desarrollo de APIs. De hecho, es uno de los mejores frameworks de desarrollo para implementar este tipo de soluciones por lo sencillo que resulta y las facilidades que ofrece.

```
+api-peliculas
|_+ app
  |_+ tests
    |_ __init__.py
    |_ test_patent.py # Testing de patente
  |_+ common
    |_ __init__.py
    |_ error_handling.py   # Utilidades para el manejo de errores
  |_+ patent
    |_+ api
      |_ __init__.py
      |_ resources.py   # Endpoints del API
    |_ __init__.py
    |_ models.py   # Modelos
  |_ __init__.py   # Configuración de la aplicación
|_+ config         # Directorio para la configuración
  |_ __init__.py
  |_ default.py    # Configuración por defecto
|_ entrypoint.py   # Crea la instancia de la app
|_ bootstrap.sh   # Punto de instancia de la app
|_ Dockerfile
|_ .gitlab-ci.yml
```

## Recursos

```
$ pip install flask flask_restful

```

## Endpoints Registrado:

```
$ '/api/v1.0/patent/<string:patent>'
$ '/api/v1.0/patent/<int:id>'
```

## Testing

```
$ python -m unittest
```
