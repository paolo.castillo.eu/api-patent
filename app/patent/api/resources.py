
from app.common.error_handling import ObjectNotFound
from flask import jsonify, Blueprint
from flask_restful import Api, Resource
from ..model import Product

products = Product()

class IdResource(Resource):
    def get(self, id: int):
        find = [row for row in products.patent if row.id == id]
        if find is None:
            raise ObjectNotFound("Patent not exists")
        return jsonify(id=find[0].id, name=find[0].name)

class PatentResource(Resource):
    def get(self, patent: str):
        find = [row for row in products.patent if row.name == patent]
        if find is None:
            raise ObjectNotFound("Patent not exists")
        return jsonify(id=find[0].id, name= find[0].name)

# point request
patent_v1_0_bp = Blueprint("patent_v1_0_bp", __name__)

# load method API
api = Api(patent_v1_0_bp)

api.add_resource(PatentResource, '/api/v1.0/patent/<string:patent>', methods=["GET"], endpoint='patent_resource')
api.add_resource(IdResource, '/api/v1.0/patent/<int:id>', methods=["GET"], endpoint='id_resource')
