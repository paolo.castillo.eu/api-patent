from app.common.utility import sequence

seq = sequence()

class Product():
    def __init__(self):
        self.patent = self.create()
    
    def create(self):
        return [Patent() for x in range(0,100)]

class Patent():
    def __init__(self):
        id = next(seq)
        self.name = id
        self.id = self.setId(id)

    def setId(self, id):
        return int(id[4:]) + 1

    def __str__(self):
        return self.name
