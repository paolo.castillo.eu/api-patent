import itertools
import string


def sequence(letters=4, digits=3):
    """ [AAAA000, AAAA001, …, ZZZZ999] """
    """ AAAA000 tiene ID =1, AAAA001 tiene ID = 2 y sucesivamente. """
    alpha = [string.ascii_uppercase] * letters
    digit = [string.digits] * digits

    for i in itertools.product(*alpha):
        for j in itertools.product(*digit):
            yield "".join(i + j)
    return alpha
