import os
import unittest
from app import create_App

class BaseTest(unittest.TestCase):

    def setUp(self):
        self.app = create_App(os.getenv("APP_SETTINGS_MODULE"))
        self.client = self.app.test_client()
    
    def getPatent(self):
        response = self.client.get("/api/v1.0/patent/1")
        assert response.status_code == 200
        assert response.get_data(as_text=True)

if __name__ == "__main__":
    unittest.main()



