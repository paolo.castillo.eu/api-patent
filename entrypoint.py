import os
from app import create_App

settings_module = os.getenv("APP_SETTINGS_MODULE")
app = create_App(settings_module)

