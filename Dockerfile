FROM python:3.8-slim

RUN apt update && apt install -y python-dev make build-essential libpq-dev python3-pip ssh && apt clean

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN python3 -m pip install -r /app/requirements.txt

COPY ./ /app


ENTRYPOINT ["/app/entrypoint.sh"]
