#!/bin/sh
PWD=`pwd`
echo PWD

activate(){
        cd $PWD
        export FLASK_APP="entrypoint:app"
        export FLASK_ENV="development"
        export APP_SETTINGS_MODULE="config.default"
        flask run
}

activate
